package com.obs.dao;

import java.util.List;
import org.hibernate.Session;

public interface UserDao<T, Id> {

    public Session getSession();

    public boolean persist(T entity) throws Exception;

    public void update(T entity);

    public T findById(Id id);

    public void delete(T entity);

    public List<T> findAll();

    public void deleteAll();

    public int doActivate(String email);
}