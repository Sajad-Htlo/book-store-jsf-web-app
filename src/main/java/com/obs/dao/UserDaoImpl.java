package com.obs.dao;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.obs.model.User;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component("userDao")
public class UserDaoImpl implements UserDao<User, Integer>, Serializable {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public Session getSession() {
        session = sessionFactory.openSession();
        return session;
    }

    public boolean persist(User entity) throws MySQLIntegrityConstraintViolationException {
        String success = getSession().save(entity).getClass().getName();
        return success != null;
    }

    public void update(User entity) {
        session.update(entity);
    }

    public User findById(Integer id) {
        User foundUser = (User) session.get(User.class, id);
        return foundUser;
    }

    public void delete(User entity) {
        session.delete(entity);
    }

    public List<User> findAll() {
        List<User> allUsers = session.createQuery("from User").list();
        return allUsers;
    }

    public void deleteAll() {
        List<User> allUsers = findAll();
        for (User eachUser : allUsers) {
            delete(eachUser);
        }
    }

    public int doActivate(String email) {
        SQLQuery sqlQuery = getSession().createSQLQuery("UPDATE  USER SET activation= TRUE WHERE email=:email");
        sqlQuery.setString("email", email);
        return sqlQuery.executeUpdate();
    }
}

