package com.obs.dao;

import com.obs.model.Orders;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
public class OrderDaoImpl implements OrderDao<Orders, Integer>, Serializable {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public OrderDaoImpl() {
    }

    public Session getSession() {
        session = sessionFactory.openSession();
        return session;
    }

    public boolean persist(Orders entity) {
        return (sessionFactory.openSession().save(entity).getClass().getName() != null);
    }

    public void update(Orders entity) {
        getSession().update(entity);
    }



    public Orders findById(String id) {
        Orders foundOrders = (Orders) sessionFactory.getCurrentSession().get(Orders.class, Integer.parseInt(id)); // should be Integer
        return foundOrders;
    }

    public void delete(Orders entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }




    public List<Orders> findAll() {
        List<Orders> allOrderses = getSession().createQuery("from Orders").list();
        return allOrderses;
    }

    public void deleteAll() {
        List<Orders> allOrderses = findAll();
        for (Orders eachOrders : allOrderses) {
            delete(eachOrders);
        }
    }

    public void deleteOrderByID(String oID) {
        System.out.println("in deleteOrderByID(), orderID: " + oID);
        Query query = getSession().createQuery("delete from orders where orderID =:orderId");
        query.setString("orderId", oID);
        int resultOfDelete = query.executeUpdate();
        System.out.println("result of delete " + resultOfDelete);
    }
}
