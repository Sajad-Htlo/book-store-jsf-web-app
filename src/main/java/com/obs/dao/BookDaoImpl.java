package com.obs.dao;


import com.obs.model.Book;

import java.io.Serializable;
import java.util.List;

import com.obs.model.OrderItem;
import org.hibernate.*;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.Projection;
import org.springframework.stereotype.Component;

@Component
public class BookDaoImpl implements BookDao<Book, Integer>, Serializable {


    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public Session getSession() {
        session = sessionFactory.openSession();
        return session;
    }

    public boolean persistBook(Book entity) {
        String success = getSession().save(entity).getClass().getName();
        System.out.println("success if persistBook(): " + success);
        return success != null;
    }

    public void update(Book entity) {

    }

    public Book findByIsbn(String isbn) {
        System.out.println("in book dao,findBook(), isbn= " + isbn);
        Query query = getSession().createQuery("from Book b where b.isbn= :isbn");
        query.setString("isbn", isbn);

        List<Book> foundBook = query.list();
        System.out.println("size of foundBook list in book Dao: " + foundBook.size());
        if (foundBook.size() > 0) {
            return foundBook.get(0);
        }
        return null;
    }


    public double getBookPrice(String isbn) {
        System.out.println("isbn in dao BookPrice(): " + isbn);
        List<Double> priceList = getSession().createQuery("select cost from Book b where b.isbn = :isbn").setString("isbn", isbn).list();
        System.out.println("price list size: " + priceList.size());
        if (priceList.size() > 0) {
            return priceList.get(0);
        } else return 0;
    }

    public List<OrderItem> getUserCardWithCriteria(String userId) {

        Criteria c = getSession().createCriteria(OrderItem.class, "oi");
        c.createAlias("oi.book", "book");
        c.createAlias("oi.orders", "or");
        c.createAlias("or.user", "usr");

        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("or.id"));
        proList.add(Projections.property("or.orderDate"));
        proList.add(Projections.property("book.title"));
        proList.add(Projections.property("book.author"));
        proList.add(Projections.property("book.cost"));
        proList.add(Projections.property("oi.quantity"));
        proList.add(Projections.property("oi.totalPrice"));
        c.setProjection(proList);

        c.add(Restrictions.eq("usr.id", Integer.parseInt(userId)));
        return c.list();
    }

    public Query findByTitle(String searchItem) {
        System.out.println("in findByTitle() dao, received: " + searchItem);
        Query query = getSession().createQuery("select title,author,cost,description,isbn from Book b where b.title like :searchTitle");
        query.setString("searchTitle", "%" + searchItem + "%");

        return query;
    }



}
