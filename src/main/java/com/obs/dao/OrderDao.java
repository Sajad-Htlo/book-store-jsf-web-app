package com.obs.dao;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface OrderDao<T, id extends Serializable> {

    public boolean persist(T entity);

    public Session getSession();

    public void update(T entity);

    public T findById(String id);

    public void delete(T orderID);

    public List<T> findAll();

    public void deleteAll();

    public void deleteOrderByID(String oID);
}