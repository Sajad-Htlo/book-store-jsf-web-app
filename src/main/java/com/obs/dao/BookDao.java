package com.obs.dao;


import com.obs.model.Book;
import com.obs.model.OrderItem;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.util.List;

public interface BookDao<T, id> {

    public boolean persistBook(T entity);

    public void update(T entity);

    public T findByIsbn(String isbn);

    public Query findByTitle(String title);

    public double getBookPrice(String isbn);

//    public SQLQuery getOrderAndBookInfo(String uID);

//    public List<Book> findAllBooks();

    public List<OrderItem> getUserCardWithCriteria(String userId);
//    public void titleOrderId();

}
