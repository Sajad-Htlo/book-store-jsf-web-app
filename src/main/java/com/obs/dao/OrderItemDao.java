package com.obs.dao;


import org.hibernate.Session;

public interface OrderItemDao<T, id> {

    public boolean persist(T entity);

}