package com.obs.dao;


import com.obs.model.OrderItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class OrderItemDaoImpl implements OrderItemDao<OrderItem, Integer>, Serializable {

    @Autowired
    private SessionFactory sessionFactory;

    private Session session;

    public boolean persist(OrderItem entity) {
        return getSession().save(entity).getClass().getName() != null;
    }


    public Session getSession() {
        session = sessionFactory.openSession();
        return session;
    }
}