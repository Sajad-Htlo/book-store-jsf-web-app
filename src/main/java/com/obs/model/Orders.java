package com.obs.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Orders implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false)
    private Date orderDate;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private User user;

    @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL)
    //Adding the mappedBy attribute makes it the inverse side of the ManyToOne association from OrderItem to Orders
    private Set<OrderItem> orderItems = new HashSet<OrderItem>();


    public Orders() {
    }

    public Orders(Date orDate, User currentUser) {
        this.orderDate = orDate;
        this.user = currentUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer orderID) {
        this.id = orderID;
    }

    public Date getDate() {
        return orderDate;
    }

    public void setDate(Date date) {
        this.orderDate = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public String toString() {
        return "order Date: " + getDate() + ", orderUser: " + getUser();
    }
}
