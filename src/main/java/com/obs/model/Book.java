package com.obs.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Book implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String author;
    @Column(nullable = false)
    private String isbn;
    private String description;
    @Column(nullable = false)
    private double cost;


    public Book() {
    }

    public Book(String title, String author, String description, double cost) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.cost = cost;
    }

    public Book(String title, String author , double cost) {
        this.title = title;
        this.author = author;
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book: " + this.getId() + ", " + this.title + ", " + this.author + ", " + this.getIsbn() + ", cost: " + getCost();
    }
}
