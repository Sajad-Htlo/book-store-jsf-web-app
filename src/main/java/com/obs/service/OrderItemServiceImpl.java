package com.obs.service;

import com.obs.dao.OrderItemDao;
import com.obs.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;

@Service
public class OrderItemServiceImpl implements OrderItemService<OrderItem, Integer>, Serializable {

    @Autowired
    private OrderItemDao orderItemDao;

    @Transactional
    public void addOrderItem(OrderItem entity) {
        orderItemDao.persist(entity);
    }

    public void deleteOrderItem() {
    }

    public void editOrderItem() {
    }
}
