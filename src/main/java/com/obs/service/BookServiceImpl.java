package com.obs.service;

import com.obs.dao.BookDao;
import com.obs.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Service
public class BookServiceImpl implements BookService<Book, Integer>, Serializable {

    @Autowired
    private BookDao bookDao;

    @Transactional
    public boolean insertBook(Book book) {
        boolean result;
        result = bookDao.persistBook(book);
        return result;
    }

    @Transactional
    public Book findBook(String isbn) {
        System.out.println("in findBook(), isbn= " + isbn);
        Book foundBook = (Book) bookDao.findByIsbn(isbn);
        System.out.println("in service findBook(), foundBook: " + foundBook);
        return foundBook;
    }

    public void deleteBook(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBook(Book entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Book findBookByUserPass(String email, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    public double bookPrice(String isbn) {
        System.out.println("isbn in service bookPrice(): " + isbn);
        double result = bookDao.getBookPrice(isbn);
//        System.out.println("book price in service(): " + result);
        return result;
    }
}
