package com.obs.service;

import com.obs.dao.OrderDaoImpl;
import com.obs.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService<Orders, Integer>, Serializable {

    @Autowired
    private OrderDaoImpl orderDao;

    public OrderServiceImpl() {

    }

    @Transactional
    public void addOrder(Orders entity) {
        orderDao.persist(entity);
    }

    @Transactional
    public void deleteOrder(String oid) {
        Orders ordersToDelete = orderDao.findById(oid);
        System.out.println("in deleteOrder() , orders To Delete: " + ordersToDelete); // correct
        orderDao.delete(ordersToDelete);
    }

    @Transactional
    public void updateOrder(Orders entity) {
        orderDao.update(entity);
    }

    @Transactional
    public Orders findOrderById(String id) {
        Orders foundOrders = orderDao.findById(id);
        return foundOrders;
    }

    @Transactional
    public List<Orders> findAllOrders() {
        List<Orders> allOrderses = orderDao.findAll();
        return allOrderses;
    }

    @Transactional
    public void deleteAllOrders() {
        orderDao.deleteAll();
    }

    @Transactional
    public void editOrder() {
    }
}
