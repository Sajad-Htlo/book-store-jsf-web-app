package com.obs.service;

import com.obs.dao.UserDao;
import com.obs.model.User;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService<User, Integer>, Serializable {

    @Autowired
    @Qualifier("userDao")
    private UserDao userDao;

    @Transactional()
    public boolean insertUser(User entity){
        boolean result;
        try {
            result = userDao.persist(entity);
        } catch (Exception e) {
            return false;
        }
        return result;
    }

    @Transactional
    public boolean userExists(String password, String email) {
        Query query = userDao.getSession().createQuery("from User where password= :password and email= :email");
        query.setString("password", password);
        query.setString("email", email);
        if (query.uniqueResult() != null) {
            System.out.println("Login Authentication Successful");
            return true;
        }
        return false;
    }


    @Transactional()
    public boolean emailExists(String value) {
        Query query = userDao.getSession().createQuery("from User where email=:email");
        query.setString("email", value);
        if (query.uniqueResult() != null) {
            return true;
        }
        return false;
    }

    @Transactional()
    public String getUserUsername(String email) {
        Query query = userDao.getSession().createQuery("select username from User where email= :email");
        query.setString("email", email);

        List<String> usernameList = query.list();
        if (usernameList.size() > 0) {
            return usernameList.get(0);
        }
        return null;
    }

    @Transactional()
    public void deleteUser(Integer id) {
        User userToDelete = (User) userDao.findById(id);
        userDao.delete(userToDelete);
    }

    @Transactional()
    public void updateUser(User entity) {
        userDao.update(entity);
    }

    @Transactional()
    public User findUserById(Integer id) {
        User foundUser = (User) userDao.findById(id);
        return foundUser;
    }

    @Transactional()
    public List<User> findAllUsers() {
        List<User> allUsers = userDao.findAll();
        return allUsers;
    }

    @Transactional()
    public void deleteAllUsers() {
        userDao.deleteAll();
    }

    @Transactional()
    public User findUserByUserPass(String email, String password) {
        Query query = userDao.getSession().createQuery("from User where email= :email and password= :password");
        query.setString("email", email);
        query.setString("password", password);
        if (query.uniqueResult() != null) { // if user exists
            User user = (User) query.uniqueResult();
            return user;
        }
        return null;
    }

    @Transactional
    public int resetPassInDB(String userEmail, String newPass) {
        System.out.println("in resetPassInDB(), email:" + userEmail + " ," + "new pass: " + newPass);
        Query query2 = userDao.getSession().createQuery("update User set password = :password where email = :email ");
        query2.setParameter("password", newPass);
        query2.setParameter("email", userEmail);

        int result = query2.executeUpdate();
        System.out.println("result: " + result);
        return result;
    }

    public boolean activateUser(String email) {
        System.out.println("in activateUser service, email: " + email);
        return (userDao.doActivate(email) > 0);
    }

}