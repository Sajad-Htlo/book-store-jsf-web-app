package com.obs.service;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

public interface UserService<T, id extends Serializable> {

    public boolean emailExists(String value);

    public boolean userExists(String password, String email);

    public String getUserUsername(String email);

    public boolean insertUser(T entity);

    public void deleteUser(id id);

    public void updateUser(T entity);

    public T findUserById(id id);

    public List<T> findAllUsers();

    public void deleteAllUsers();

    public T findUserByUserPass(String email, String password);

    public int resetPassInDB(String email, String pass);

    public boolean activateUser(String email);
}