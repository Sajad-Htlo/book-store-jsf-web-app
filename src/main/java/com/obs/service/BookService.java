package com.obs.service;


import java.util.List;

public interface BookService<T, id> {

    public boolean insertBook(T book);

    public void deleteBook(id id);

    public void updateBook(T entity);

    public T findBook(String isbn);

    public T findBookByUserPass(String email, String password);

    public double bookPrice(String isbn);

}