package com.obs.service;

public interface OrderItemService<T, id> {

    public void addOrderItem(T entity);

    public void deleteOrderItem();

    public void editOrderItem();
}