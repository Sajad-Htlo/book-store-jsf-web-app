package com.obs.service;

public interface OrderService<T, id> {

    public void addOrder(T oid);

    public void deleteOrder(String oid);

    public void editOrder();
}
