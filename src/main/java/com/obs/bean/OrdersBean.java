package com.obs.bean;

import com.obs.model.Book;
import com.obs.model.Orders;
import com.obs.model.OrderItem;
import com.obs.model.User;
import com.obs.service.BookService;
import com.obs.service.OrderService;
import com.obs.service.UserService;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;

//@ManagedBean
//@SessionScoped
@Component
@Scope("session")
public class OrdersBean {

    private Book currentBook;
    private Orders orders;
    private String isbn;
    private double unitPrice;
    private int qty;

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserBean userBean;


    @PostConstruct
    public void init() {
        qty = 1;
    }

    public String selectBook() {
        isbn = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("isbn");
        System.out.println("in select book(), isbn: " + isbn);

        if (isbn != null || !isbn.equals("null")) {
            currentBook = ((Book) bookService.findBook(isbn));
            System.out.println("current Book: " + getCurrentBook());

            if (getCurrentBook() != null) {
                System.out.println("Book found, Go Books.xhtml...");
                return "Books?isbn=" + isbn + "faces-redirect=true";
            } else {
                return "";
            }
        }
        return "home?faces-redirect=true";
    }

    public void orderBook() throws IOException {
        System.out.println("in orderBook(), Qty is: " + this.qty + ", isbn: " + isbn);
        userBean.setCurrentUser(userBean.getCurrentUser());

        if (userBean.getCurrentUser() == null) {
            System.out.println("null user, call the dialog");
            RequestContext rc = RequestContext.getCurrentInstance();
            rc.execute("PF('dlg1').show()");
        } else {
            // user is logged in
            System.out.println("current User not null");
            storeOrderInDB(isbn);
            setQty(1);
            FacesContext.getCurrentInstance().getExternalContext().redirect("MyCard.xhtml");
        }
    }

    public void loginDialog() throws IOException {
        System.out.println("isbn in login dialog: " + isbn);

        byte[] byteHash = hashPassword(userBean.getPassword());
        String hashedPassword = new String(byteHash);

        userBean.setCurrentUser((User) userService.findUserByUserPass(userBean.getEmail(), hashedPassword));

        if (userBean.getCurrentUser() != null) {
            System.out.println("Login Dialog Successful,Store in DB() and Redirect to My card...");
            storeOrderInDB(isbn);
            setQty(1);
            FacesContext.getCurrentInstance().getExternalContext().redirect("MyCard.xhtml");
        } else {
            System.out.println("Login Dialog Failed");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "دوباره سعی کنید"));
        }
    }

    @Transactional
    public void storeOrderInDB(String isbn) {
        currentBook = (Book) bookService.findBook(isbn);
        unitPrice = currentBook.getCost();
        System.out.println("In storeOrderInDB(), Book: " + getCurrentBook() + " , isbn: " + isbn + " , unit price: " + unitPrice);

        // Orders object
        orders = new Orders();
        orders.setDate(currentDate());
        orders.setUser(userBean.getCurrentUser());

        // OrderItem Object
        OrderItem orderItem = new OrderItem();
        orderItem.setBook(currentBook);
        orderItem.setQuantity(qty);
        orderItem.setTotalPrice(getTotalCost(qty, unitPrice));
        orderItem.setOrders(orders);
        System.out.println("current orderItem in storeOrderInDB(): " + orderItem);

        orders.getOrderItems().add(orderItem);

        orderService.addOrder(orders); // persist to orders table

        System.out.println("storeOrderInDB() done.");
    }

    public double getTotalCost(int qty, double unitPrice) {
        System.out.println("in getTotalCost(): qty= " + qty + " , unit price= " + unitPrice);
        return qty * unitPrice;
    }

    public byte[] hashPassword(String password) {
        System.out.println("plain text: " + password);
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in registration: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in registration: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }

    public Book getCurrentBook() {
        return currentBook;
    }

    public void setCurrentBook(Book currentBook) {
        this.currentBook = currentBook;
    }

    public Date currentDate() {
        Date currentDate = new Date();
        return currentDate;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
