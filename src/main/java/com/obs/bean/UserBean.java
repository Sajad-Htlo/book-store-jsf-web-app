package com.obs.bean;


import com.obs.model.User;
import com.obs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

@Component
@Scope("session")
public class UserBean implements Serializable {

    private String username;
    private String password;
    private String email;
    private User currentUser;

    @Autowired
    private UserService userService;

    @PostConstruct
    public void init() {
        setUsername("");
        setPassword("");
        setEmail("");
    }

    public String login() {
        String hashedPassword = new String(hashPassword(password));
        currentUser = (User) userService.findUserByUserPass(email, hashedPassword);
        if (getCurrentUser() != null) {
            System.out.println("Login Successful");
            return "home?faces-redirect=true";
        } else {
            System.out.println("Login Failed");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", " حسابی یافت نشد، دوباره امتحان کنید "));
            setEmail(null);
            setPassword(null);
            currentUser = null;
            return null;
        }
    }

    public String logout() {
        System.out.println("in logout()...");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        currentUser = null;
        return "/home.xhtml?faces-redirect=true";
    }

    public byte[] hashPassword(String password) {
        System.out.println("plain text: " + password);
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in login: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in login: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isLoggedIn() {
        return currentUser != null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public String toString() {
        return "username: " + getUsername() + " password: " + getPassword() + ", email: " + getEmail();
    }
}