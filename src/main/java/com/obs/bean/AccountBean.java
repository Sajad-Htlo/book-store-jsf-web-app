package com.obs.bean;

import com.obs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

@Component
public class AccountBean {

    private String pass1;
    private String pass2;
    private String email;

    @Autowired
    private UserService userService;


    public void doReset() throws IOException {
        setEmail(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("email"));
        System.out.println("pass1: " + pass1 + " , pass2: " + pass2 + " ,email: " + getEmail());
        if (pass1.equalsIgnoreCase(pass2)) {
            System.out.println("Matched");

            byte[] byteHash = hashPassword(pass1);
            String hashedPassword = new String(byteHash);

            if (userService.resetPassInDB(getEmail(), hashedPassword) > 0) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                Flash flash = facesContext.getExternalContext().getFlash();
                flash.setKeepMessages(true);
                flash.setRedirect(true);
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", " رمز عبور شما با موفقیت تغییر یافت "));
                facesContext.getExternalContext().redirect("login.xhtml");
            }

        } else {
            System.out.println("not matched");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", " رمز عبور وارد شده با هم مطابقت ندارد "));
        }
    }

    public byte[] hashPassword(String password) {
        System.out.println("plain text: " + password);
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in registration: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in registration: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass1() {
        return pass1;
    }

    public void setPass1(String pass1) {
        this.pass1 = pass1;
    }

    public String getPass2() {
        return pass2;
    }

    public void setPass2(String pass2) {
        this.pass2 = pass2;
    }
}
