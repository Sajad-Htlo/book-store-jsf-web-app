package com.obs.bean;


import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

//@ManagedBean
//@SessionScoped
@Component
public class AccountRecoveryBean {

    private String email;
    String link2 = ">Reset Password</a>";
    String message = "Click link below to reset your password <br/><br/>" +
            "<a href= http://localhost:8080/resetPass.xhtml?email=";


    public void sendEmail() throws Exception {
        boolean sendResult = false;
        System.out.println("in sendEmail(),for: " + email); //ok
        sendResult = sendHtmlEmail(getEmail(), "Reset Password", message);
        if (sendResult) {
            email = "";
            System.out.println("Email sent successful.");

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", ". ایمیلی به شما ارسال شد، صندوق ورودی خود را چک کنید "));
        } else {
            System.out.println("Email sent failed.");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", " مشکلی در فرستادن ایمیل بوجود آمده، دوباره سعی کنید "));
        }
    }

    public boolean sendHtmlEmail(String toAddress, String subject, String message) {

        String host = "smtp.gmail.com";
        String port = "587";
        final String userName = "sajjad.htlo@gmail.com";
        final String password = "shahin45634563";

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(userName));
            InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            // set plain text message
            System.out.println("in send html email: getEmail1(): " + getEmail());
            msg.setContent((message + getEmail() + link2), "text/html");
            // sends the e-mail
            Transport.send(msg);
            return true;
        } catch (AddressException e) {
            return false;
        } catch (MessagingException e) {
            return false;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
