package com.obs.bean;

import com.obs.dao.BookDao;
import com.obs.model.OrderItem;
import com.obs.service.OrderService;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

//@ManagedBean
//@ViewScoped
@Component
@Scope("session")
public class MyCardBean implements Serializable {

    @Autowired
    private UserBean userBean;
    @Autowired
    private BookDao bookDao;

    @Autowired
    private OrderService orderService;

    private Object[] selectedOrder;
    private List<OrderItem> orderBookInfo;
    private String orderIdToDelete = "";

    @Transactional
    public List<OrderItem> userCard(String uID) {
        System.out.println("in userCard(), userId: " + userBean.getCurrentUser().getId());
        orderBookInfo = (List<OrderItem>) bookDao.getUserCardWithCriteria(uID);
        System.out.println("orderBookInfo, size: " + orderBookInfo.size());
        return orderBookInfo;
    }

    public void deleteOrder() {
        System.out.println("in delete() of myCardBean, orderId To Delete: " + orderIdToDelete);
        orderService.deleteOrder(orderIdToDelete);
    }

    public void setSelectedOrder(Object[] selectedOrder) {
        System.out.println("row: orderID: " + selectedOrder[0] + " ,quantity: " + selectedOrder[5]);
        orderIdToDelete = String.valueOf(selectedOrder[0]);
        System.out.println("orderIdToDelete assigned, value is: " + orderIdToDelete);
        this.selectedOrder = selectedOrder;
    }

    public void onRowSelect(SelectEvent event) {
        System.out.println("row selected.");
    }

    public void onRowUnselect(UnselectEvent event) {
        System.out.println("row Unselected");
    }

    public Object[] getSelectedOrder() {
        return selectedOrder;
    }

    public String addExtraOrder() {
        return "home.xhtml";
    }

    public List<OrderItem> getOrderBookInfo() {
        return orderBookInfo;
    }

    public void setOrderBookInfo(List<OrderItem> orderBookInfo) {
        this.orderBookInfo = orderBookInfo;
    }

    public String getOrderIdToDelete() {
        return orderIdToDelete;
    }

    public void setOrderIdToDelete(String orderIdToDelete) {
        this.orderIdToDelete = orderIdToDelete;
    }
}
