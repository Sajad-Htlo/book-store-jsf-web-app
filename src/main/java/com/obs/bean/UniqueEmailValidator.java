package com.obs.bean;

import com.obs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Scope("request")
public class UniqueEmailValidator implements Validator {

    @Autowired
    private UserService userService;

    public void validate(FacesContext fc, UIComponent uic, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        String email = (String) value;

        boolean exists = userService.emailExists(email);
        System.out.println("result for email: " + email + " :" + exists);
        if (exists) {
            throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "این ایمیل قبلا ثبت شده است", null));
        }
    }
}