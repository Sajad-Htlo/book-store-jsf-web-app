package com.obs.bean;

import com.obs.model.User;
import com.obs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Date;
import java.util.Properties;

@Component
public class RegistrationBean {

    @Autowired
    private UserService userService;
    private boolean regFinished;
    private String username;
    private String password;
    private String email1;
    private String email2;

    String message = "برای فعال سازی حساب خود روی لینک زیر کلیک کنید <br/><br/>" +
            "<a href= http://localhost:8080/ActivationSuccessful.xhtml?email1=";
    String link = ">Activate Account</a>";

    public void registration() throws IOException {

        if (email1.equalsIgnoreCase(email2)) {

            byte[] byteHash = hashPassword(password);
            String hashedPassword = new String(byteHash);

            User newUser = new User(username, hashedPassword, email1, false);
            boolean userRegistered = userService.insertUser(newUser);

            if (userRegistered == true) {
                System.out.println("Registration done, sending email1 to: " + email1 + " ...");
                if (sendHtmlEmail(email1, "Account Activation", (message + email1 + link))) {
                    System.out.println("Email sent successfully.");
                    setUsername("");
                    setPassword("");
                    setEmail1("");
                    setEmail2("");

                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    Flash flash = facesContext.getExternalContext().getFlash();
                    flash.setKeepMessages(true);
                    flash.setRedirect(true);
                    facesContext.addMessage("regSucForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", " لینک فعال سازی به ایمیل شما ارسال شد "));
                    facesContext.getExternalContext().redirect("RegistrationSuccessful.xhtml");
                } else {
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, " مشکلی در ارسال ایمیل به شما داریم", "شما ثبت نام شده اید");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    setUsername("");
                    setPassword("");
                    setEmail1("");
                    setEmail2("");
                }

            } else {
                System.out.println("Registration Failed ");
                setUsername("");
                setPassword("");
                setEmail1("");
                setEmail2("");
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "دوباره امتحان کنید", "مشکلی در ثبت نام شما وجود دارد");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ایمیل های وارد شد با هم مطابقت ندارند", ""));
        }
    }

    public void activateUser() {
        String activateEmail = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("email1");
        System.out.println("received email1: " + activateEmail);
        if (userService.activateUser(activateEmail)) {
            System.out.println("Activation successful");
        } else {
            System.out.println("Activation Failed");
        }
    }

    public boolean sendHtmlEmail(String toAddress, String subject, String message) {

        String host = "smtp.gmail.com";
        String port = "587";
        final String userName = "sajjad.htlo@gmail.com";
        final String password = "shahin45634563";

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(userName));
            InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            // set plain text message
            msg.setContent(message, "text/html; charset=UTF-8");
            // sends the e-mail
            Transport.send(msg);
            return true;
        } catch (AddressException e) {
            return false;
        } catch (MessagingException e) {
            return false;
        }
    }

    public byte[] hashPassword(String password) {
        System.out.println("plain text: " + password);
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in registration: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in registration: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public boolean isRegFinished() {
        return regFinished;
    }

    public void setRegFinished(boolean regFinished) {
        this.regFinished = regFinished;
    }
}
