package com.obs.bean;

import com.obs.dao.BookDao;
import com.obs.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SearchBean {

    private String searchKeyword;
    private List<Book> searchBookList;

    @Autowired
    private BookDao bookDao;

    @Transactional
    public String search() throws IOException {
        System.out.println("in search(), keyword: " + getSearchKeyword() + "bookDao:" + bookDao);
        if (getSearchKeyword().length() > 0) {
            searchBookList = new ArrayList<Book>();
            searchBookList = bookDao.findByTitle(getSearchKeyword()).list();
            System.out.println("size of list: " + searchBookList.size());
            if (searchBookList.size() == 0) { // no result found
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "نتیجه ای یافت نشد"));
                setSearchKeyword("");
                return "";
            } else {
                setSearchKeyword("");
                return "search?faces-redirect=true";
            }
        }
        System.out.println("search Keyword is empty");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "عنوان کتاب را وارد کنید"));
        return "";
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public List<Book> getSearchBookList() {
        return searchBookList;
    }

    public void setSearchBookList(List<Book> searchBookList) {
        this.searchBookList = searchBookList;
    }
}
