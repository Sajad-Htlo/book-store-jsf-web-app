This project demonstrates a maven based integration of `Spring core`, `Hibernate` and `JSF` frameworks.

Technologies used:

Tomcat 8.0

Maven

MySQL 5.1 (Database name: `obs` , Username: `root` , Password: `2323` )

Hibernate 4.3

Spring 4.1

Jsf 2.2

Primefaces 5.0

Here is the simplified application workflow:

`User registration` > `Login` > `Home page` > `Book Info Page` > `Order Book` > `ShoppingCard`

`Login` > `Account Recovery` > `Send Email To User` > `Reset Password` > `Login`

`Registration` > `Send Confirmation Email` > `Login`

Book table initilizes in each project deployment with `initialScript.sql` file, since the `hibernate.hbm2ddl.auto` value in the `applicationContext.xml` is set to `create`. You can change it to `updtae` if you want.

There is no special features/authorities for `confirmed users` yet.